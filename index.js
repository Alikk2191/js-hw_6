let createNewUser = function() {
    let name = prompt("Введіть ім'я");
    let surname = prompt("Введіть прізвище");
    let birthdate = prompt("Введіть дату народження (формат dd.mm.yyyy)");
  
    let parts = birthdate.split(".");
    let day = parseInt(parts[0]);
    let month = parseInt(parts[1]) - 1; 
    let year = parseInt(parts[2]);
  
    let newUser= {
      firstName: name,
      lastName: surname,
      birthDate: new Date(year, month, day),
      getAge() {
        let today = new Date();
        let age = today.getFullYear() - this.birthDate.getFullYear();
        let monthDiff = today.getMonth() - this.birthDate.getMonth();
        if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < this.birthDate.getDate())) {
          age--;
        }
        return age;
      },
      getPassword() {
        let year = this.birthDate.getFullYear().toString();
        let firstLetter = this.firstName.charAt(0).toUpperCase();
        let lastName = this.lastName.toLowerCase();
        return `${firstLetter}${lastName}${year}`;
      },
    };
  
    return newUser;
  }
  
  const user = createNewUser();
  
  console.log(user);
  console.log(`Age: ${user.getAge()}`);
  console.log(`Password: ${user.getPassword()}`);
  